<?php 
namespace cat_crash\valorpaytech;

class Valorpaytech {

	private $transport=null;
	private $mode='test';
	private $urls=[
		'live'=>'https://securelink.valorpaytech.com:4430',
		'test'=>'https://securelinktest.valorpaytech.com:4430'
	];

	private $APP_ID=null;
	private $API_KEY=null;
	private $EPI=null;

	private $client=null;

	private $response=null;

	public function __construct($APP_ID,$API_KEY,$EPI,$mode='test'){
		$this->setCredentials($APP_ID,$API_KEY,$EPI);
		$this->setMode($mode);

		$this->client = new \GuzzleHttp\Client();

	}

	public function setMode($mode){
		switch($mode){
			default:
			case 'test':
				$this->mode='test';
			break; 

			case 'live':
				$this->mode='live';
			break;

		}
	}

	public function setCredentials($APP_ID,$API_KEY,$EPI){
		$this->APP_ID=$APP_ID;
		$this->API_KEY=$API_KEY;
		$this->EPI=$EPI;
	}


	public function authorize(array $_payment){		
		
		$_fiels=[
			'cardnumber' => $_payment['cardnumber'],
			'cardholdername' => $_payment['cardholdername'],
			'cvv' => $_payment['cvv'],
			'expirydate' => $_payment['expirydate'], //MMYY

			'address1' => $_payment['address1'],
			'address2' => isset($_payment['address2'])?$_payment['address2']:null,
			'city' => $_payment['city'],
			'state' => $_payment['state'],
			'zip' => $_payment['zip'],
			'phone' => $_payment['phone'],
			'ip' => $_payment['ip'],
			'email' => $_payment['email'],

			'shipping_country'=> $_payment['shipping_country'], //USA
			'billing_country'=> $_payment['billing_country'], //USA

			'orderdescription' => $_payment['orderdescription'],
			'invoicenumber' => $_payment['invoicenumber'],

			'amount' => $_payment['amount'],
			'surchargeIndicator' => $_payment['surchargeIndicator'],
			'status' => 'Y'
		];

		return $this->auth($_fiels);
	}

	//Authonly transaction performs the authorization by getting the card number and other details, Settlement can be manually done on each transaction 

	private function auth(array $_fiels){
		$txn_type = 'auth';

		$authData=[
			'appid' => $this->APP_ID,
			'appkey' => $this->API_KEY,
			'epi' => $this->EPI,
			'txn_type' => $txn_type,
		];

		$_payload=array_merge($authData, $_fiels);

		return $this->executeRequest($_payload);
	}

	public function executeRequest(array $_payload){
		$client=$this->client;

		try {

		    $response= $client->post($this->urls[$this->mode],['form_params'=>$_payload]);

		} catch (RequestException $e) {

		    throw new \Exception(Psr7\str($e->getRequest()));

		    if ($e->hasResponse()) {
		    	throw new Exception(Psr7\str($e->getResponse()));
		    }
		}

		$this->response=json_decode($response->getBody(),true);

		return $this;
	}

	public function getResponse(){
		$response_data=$this->response;

		if(is_array($response_data)){
			return new Response($response_data);
		}

		return null;
	}

	public function getError(){
		$response_data=$this->response;
		if ($response_data['error_no'] !== 'S00'){
			return new Error($response_data);
		}
		return false;

	}

	public function isSuccess(){

		$response_data=$this->response;

		if(is_array($response_data)){
			if ($response_data['error_no'] == 'S00') {

				return true;

			} else {

				return false;
			}

		} else {

			return false;
		}

	}

}
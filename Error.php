<?php 

namespace cat_crash\valorpaytech;

class Error {
	private $error_no;
	private $mesg;
	private $desc;

	public function __construct($_array){
		if(isset($_array['error_no'])){
			$this->setErrorNo($_array['error_no']);
		}

		if(isset($_array['mesg'])){
			$this->setMesg($_array['mesg']);
		}

		if(isset($_array['desc'])){
			$this->setDesc($_array['desc']);
		}

		return $this;
	}


	private function setErrorNo($error_no){
		$this->error_no=$error_no;
	}

	private function setMesg($mesg){
		$this->mesg=$mesg;
	}

	private function setDesc($desc){
		$this->desc=$desc;
	}

	public function getErrorNo(){
		return $this->error_no;
	}

	public function getMesg(){
		return $this->mesg;
	}

	public function getDesc(){
		return $this->desc;
	}

}


?>
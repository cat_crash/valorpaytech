<?php 

namespace cat_crash\valorpaytech;

class Response {
	private $error_no;
	private $msg;
	private $desc;
	private $approval_code;
	private $amount;
	private $rrn;
	private $txnid;
	private $additional_info;

	public function __construct($_array){
		if(isset($_array['error_no'])){
			$this->setErrorNo($_array['error_no']);
		}

		if(isset($_array['msg'])){
			$this->setMsg($_array['msg']);
		}

		if(isset($_array['desc'])){
			$this->setDesc($_array['desc']);
		}

		if(isset($_array['approval_code'])){
			$this->setApprovalCode($_array['approval_code']);
		}

		if(isset($_array['amount'])){
			$this->setAmount($_array['amount']);
		}

		if(isset($_array['rrn'])){
			$this->setRrn($_array['rrn']);
		}

		if(isset($_array['txnid'])){
			$this->setTxnid($_array['txnid']);
		}

		if(isset($_array['additional_info'])){
			$this->setAdditionalInfo($_array['additional_info']);
		}

		return $this;
	}


	private function setErrorNo($error_no){
		$this->error_no=$error_no;
	}

	private function setMsg($msg){
		$this->msg=$msg;
	}

	private function setDesc($desc){
		$this->desc=$desc;
	}

	private function setApprovalCode($approval_code){
		$this->approval_code=$approval_code;
	}

	private function setAmount($amount){
		$this->amount=$amount;
	}

	private function setRrn($rrn){
		$this->rrn=$rrn;
	}

	private function setTxnid($txnid){
		$this->txnid=$txnid;
	}

	private function setAdditionalInfo($additional_info){
		$this->additional_info=$additional_info;
	}


	public function getErrorNo(){
		return $this->error_no;
	}

	public function getMsg(){
		return $this->msg;
	}

	public function getDesc(){
		return $this->desc;
	}

	public function getApprovalCode(){
		return $this->approval_code;
	}

	public function getAmount(){
		return $this->amount;
	}

	public function getRrn(){
		return $this->rrn;
	}

	public function getTxnid(){
		return $this->txnid;
	}

	public function getAdditionalInfo(){
		return $this->additional_info;
	}

}


?>
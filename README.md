# Valorpaytech PHP Client Library (UNOFICIAL)


## Installation

***NOTE:** Although this library may work with PHP 5.3^, it is only tested against PHP 7.2^ and we highly recommended to use PHP 7.2^. Additionally, 

### Install Client Library

**Install the Library via [Composer](http://getcomposer.org/):**

```shell
composer require cat_crash/valorpaytech
```

**OR: Manually Install the Library**

Manually download this library and require it from your project:

```php
require_once("/path/to/lib/valorpaytech.php");
```

### Install Dependencies

Run the following from the root directory of the library:

```shell
composer install
```

## Example

```php
require_once("/path/to/vendor/valorpaytech/valorpaytech.php");

$valorpaytech=new Valorpaytech('APP_ID','API_KEY','EPI','test' /* use 'live' for production */); 

            $return=$valorpaytech->authorize([
                
                'cardnumber' => '4111111111111111',
                'cardholdername' => 'John Doe',
                'cvv' => '888',
                'expirydate' => '1223', //MMYY

                'address1' => '123 Somestreet rd',
                //'address2' => $_payment['address2'],
                'city' => 'New York',
                'state' => 'NY',
                'zip' => '10101',
                'phone' => '2122345678',
                'ip' => $_SERVER['REMOTE_ADDR'],
                'email' => 'some@email.com',

                'shipping_country'=> 'USA', //USA
                'billing_country' => 'USA', 

                'orderdescription' => 'Tesla model XYZ ',

                'amount' => 10.1,
                'surchargeIndicator'=>1
                
            ]);

            if($return->isSuccess()){ //Check if request executed successfully 
               var_dump($return->getResponse());
            } else {
                var_dump($return->getError()->getMesg();
            }
```

## Documentation

ToDo

## Releasing
ToDo
